package pa.com.cajadeahorros;

import org.springframework.boot.SpringApplication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DummyOnamdApplication {

	public static void main(String[] args) {
		
		Logger logger = LogManager.getLogger(DummyOnamdApplication.class);
		 logger.error("****************************************");
		 logger.error("****************************************");
		 logger.error("****************************************");
		 logger.error("****************************************");
		 logger.error("****************************************");
		  logger.error("****************************************");
		    logger.error("in main class****************************************");
	        logger.info("info logging is printed****************************************");
	        logger.debug("logger debud is worked****************************************");
	        logger.warn("logging warn is worked****************************************");
		SpringApplication.run(DummyOnamdApplication.class, args);
	}

}
