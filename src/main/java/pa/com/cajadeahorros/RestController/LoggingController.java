package pa.com.cajadeahorros.RestController;

	import org.apache.logging.log4j.LogManager;
	import org.apache.logging.log4j.Logger;
	import org.springframework.ui.Model;
	import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
	import java.util.List;

	@RestController
	public class LoggingController {

	    private static final Logger logger = LogManager.getLogger(LoggingController.class);

	    private List<Integer> num = Arrays.asList(1, 2, 3, 4, 5);

	    @GetMapping("/log")
	    public String main(Model model) {

	        // pre-java 8
	        if (logger.isDebugEnabled()) {
	            logger.debug("Hello from Log4j 2 - num : {}", num);
	        }

	        // java 8 lambda, no need to check log level
	        logger.debug("Hello from Log4j 2 - num : {}", () -> num);

	        model.addAttribute("tasks", num);
	        return "welcome"; //view
	    }

	   // private int getNum() {
	   //     return 100;
	   // }
	
	
}
