/**
 * 
 */
package pa.com.cajadeahorros.RestController;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pa.com.cajadeahorros.entity.*;

/**
 * @author OnamD
 *
 */
@RestController
public class ServiceController {
	
	 private static final Logger log = LoggerFactory.getLogger(ServiceController.class);
	
	
	private static Map<String, Cliente> cl = new HashMap<>();
	   static {
			     Cliente cliente = new Cliente();
			     cliente.setApellidos("Díaz");
				 cliente.setDni("8-765-1309");
				 cliente.setId("1");
				 cliente.setNombres("Onam Yoel");
				 cl.put(cliente.getId(), cliente);
	      
				 Cliente cliente1 = new Cliente();
				 cliente1.setApellidos("Díaz");
				 cliente1.setDni("8-765-1309");
				 cliente1.setId("2");
				 cliente1.setNombres("Onam Yoel");
				 cl.put(cliente1.getId(), cliente1);
				 
				log.debug("Se crean estaticos los dummys");
	   }
	
	   
	   @RequestMapping(value = "/cliente/{id}", method = RequestMethod.DELETE)
	   public ResponseEntity<Object> delete(@PathVariable("id") String id) { 
	      cl.remove(id);
	      return new ResponseEntity<>("Product is deleted successsfully", HttpStatus.OK);
	   }
	   
	   @RequestMapping(value = "/cliente/{id}", method = RequestMethod.PUT)
	   public ResponseEntity<Object> updateProduct(@PathVariable("id") String id, @RequestBody Cliente cliente) { 
		   cl.remove(id);
		   cliente.setId(id);
	      cl.put(id, cliente);
	      return new ResponseEntity<>("Cliente is updated successsfully", HttpStatus.OK);
	   }
	   
	   @RequestMapping(value = "/cliente", method = RequestMethod.POST)
	   public ResponseEntity<Object> createProduct(@RequestBody Cliente cliente) {
		   cl.put(cliente.getId(), cliente);
	      return new ResponseEntity<>("Cliente is created successfully", HttpStatus.CREATED);
	   }
	   

	
	@RequestMapping(value = "/cliente")
	public ResponseEntity<Object> cliente() { 
		
		log.debug("Se crean estaticos los dummys");
	
		 return new ResponseEntity<>(cl.values(), HttpStatus.OK);
			
	
		
	}

}


