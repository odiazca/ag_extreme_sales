package pa.com.cajadeahorros;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;
import static com.google.common.base.Predicates.or;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	

	@Bean
	public Docket postsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("swaggwer.json")
				.apiInfo(apiInfo())
				.select()
				.paths(postPaths())
				.build();
	}

	private Predicate<String> postPaths() {
		return or(regex("/api/dummy.*"), regex("/api/services.*"));
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Caja Dummy API")
				.description("DummysOnam")
				.termsOfServiceUrl("http://bdahorros.cajadeahorros.pma/intranet/")
				.contact(new Contact("Onam Diaz", "http://localhost", "onam.diaz@cajadeahorros.com"))
				.license("Caja de Ahorros License")
				.licenseUrl("onam.diaz@cajadeahorros.com")
				.version("1.0")
				.build();
	}

}